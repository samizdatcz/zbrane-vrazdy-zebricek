
metrics = <[guns homicides]>
headers =
  guns: "Počet zbraní na 100 obyvatel"
  homicides: "Počet vražd střelnou zbraní na 100 000 obyvatel"

data = d3.tsv.parse ig.data.zbrane, (row) ->
  for field in metrics => row[field] = parseFloat row[field]
  row

data .= filter -> it.reduced

for datum in data => datum.slope = []

container = d3.select ig.containers.base
graphs = container.append \div .attr \class \graphs
graphElements = for let metric in metrics
  graphData = data.slice!
    ..sort (a, b) -> b[metric] - a[metric]
  for datum, index in graphData
    datum.slope.push index
    datum.slope.push index
  scale = d3.scale.linear!
    ..domain [0 graphData[0][metric]]
    ..range [0 50]
  position = if metric == "guns" then \right else \left
  decimals = if metric == "guns" then 1 else 2
  graphs.append \div .attr \class "graph #metric"
    ..append \h2 .html headers[metric]
    ..append \div .attr \class \lines
      ..selectAll \div.line .data graphData .enter!append \div
        ..attr \class -> "line" + if it.country == "Česká republika" then " cz" else ""
        ..append \div
          ..attr \class \title
          ..html -> "#{it.country} <b>#{ig.utils.formatNumber it[metric], decimals}</b>"
          ..style position, -> "#{scale it[metric]}%"
        ..append \div
          ..attr \class \bar
          ..style \width -> "#{scale it[metric]}%"
        ..on \mouseover -> highlight it
        ..on \mouseout -> downlight!

lines = container.selectAll \div.line
width = container.node!clientWidth - 2 * graphElements[0].node!.clientWidth
height = graphElements[0].node!.clientHeight

path = d3.svg.line!
  ..x (d, i) -> switch i
    | 0 => 0
    | 1 => 10
    | 2 => width - 10
    | 3 => width
  ..y -> it * 30
  ..interpolate \monotone

slopes = container.append \svg
  .attr \width width
  .attr \height height
  .selectAll \path .data data .enter!append \path
    ..attr \d -> path it.slope

highlight = (item) ->
  for graph in [lines, slopes]
    graph.classed \active -> it is item

downlight = ->
  for graph in [lines, slopes]
    graph.classed \active no
